package ma.octo.assignement.web;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.VersementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Controller
@RestController
public class VersementController {
    @Autowired
    private VersementService versementService;
    /**
     * AssignementComment:
     * rendre les routes plus claire
     * et separation of concerns
     * */
    @GetMapping("/versements")
    List<Versement> loadAll() {
        return  versementService.getAllDeposits();
    }
    @PostMapping("/ajouterVersement")
    VersementDto addVersement(@Valid @RequestBody VersementDto versementDto) throws TransactionException, CompteNonExistantException {
        return versementService.addDeposit(versementDto);
    }

}

package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@Controller
@RestController
public class UtilisateurController {
    @Autowired
    private UtilisateurService utilisateurService;
    /**
     * AssignementComment:
     * rendre le route plus claire
     * */
    @GetMapping("/utilisateurs")
    List<Utilisateur> loadAll() {
        return  utilisateurService.getAllUsers();
    }
}

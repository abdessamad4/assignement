package ma.octo.assignement.web;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.service.VirementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Controller
@RestController
public class VirementController {
    /**
     * AssignementComment:
     * rendre les routes plus claire
     * et separation of concerns
     * */
    @Autowired
    private VirementService virementService;

    @GetMapping("/virements")
    List<Virement> loadAll() {
        return  virementService.getAllVirments();
    }

    @PostMapping("/ajouterVirement")
    VirementDto addVirement(@Valid @RequestBody VirementDto virement) throws TransactionException, CompteNonExistantException {
        return virementService.addVirement(virement);
    }
}

package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;

public class VirementMapper {

    private static VirementDto virementDto;

    public static VirementDto map(Virement virement) {
        /**
         * AssignementComment:
         * une maniere plus simple pour realiser le mapping
         * */
        return new VirementDto(
                virement.getCompteEmetteur().getNrCompte(),
                virement.getCompteBeneficiaire().getNrCompte(),
                virement.getMotifVirement(),
                virement.getMontantVirement() ,
                virement.getDateExecution() );

    }
}

package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

/**
 * AssignementComment:
 * ajout du VersementMapper
 * */
public class VersementMapper {
    public static VersementDto map(Versement versement) {

        return new VersementDto(
                versement.getMontantVersement(),
                versement.getDateExecution(),
                versement.getNomComplet(),
                versement.getCompteBeneficiaire().getRib(),
                versement.getMotifVersement() );
    }
}

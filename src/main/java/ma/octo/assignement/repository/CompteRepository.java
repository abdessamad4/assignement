package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompteRepository extends JpaRepository<Compte, Long> {
  Compte findByNrCompte(String nrCompte);
  /**
   * AssignementComment:
   * ajout de la methode findByRib
   * */
  Compte findByRib(String rib);
}

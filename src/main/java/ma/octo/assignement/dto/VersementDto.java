package ma.octo.assignement.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class VersementDto {
    /**
     * AssignementComment:
     * Ajout des validation
     * */

    @DecimalMin(value = "5.0", inclusive = true)
    @DecimalMax(value = "10000", inclusive = true)
    private BigDecimal montantVersement;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "champ date est obligatoire")
    private Date dateExecution;

    @NotBlank(message = "champ nomComplet est obligatoire")
    private String nomComplet;

    @NotBlank(message = "champ ribBenificiaire est obligatoire")
    private String ribBenificiare;

    @NotBlank(message = "champ motif est obligatoire")
    private String motifVersement;

    public VersementDto(BigDecimal montantVersement, Date dateExecution, String nomComplet, String ribBenificiare, String motifVersement) {
        this.montantVersement = montantVersement;
        this.dateExecution = dateExecution;
        this.nomComplet = nomComplet;
        this.ribBenificiare = ribBenificiare;
        this.motifVersement = motifVersement;
    }

    @Override
    public String toString() {
        return "VersementDto{" +
                "montantVersement=" + montantVersement +
                ", dateExecution=" + dateExecution +
                ", nomComplet='" + nomComplet + '\'' +
                ", ribBenificiare='" + ribBenificiare + '\'' +
                ", motifVersement='" + motifVersement + '\'' +
                '}';
    }

    public BigDecimal getMontantVersement() {
        return montantVersement;
    }

    public void setMontantVersement(BigDecimal montantVersement) {
        this.montantVersement = montantVersement;
    }

    public Date getDateExecution() {
        return dateExecution;
    }

    public void setDateExecution(Date dateExecution) {
        this.dateExecution = dateExecution;
    }

    public String getNomComplet() {
        return nomComplet;
    }

    public void setNomComplet(String nomComplet) {
        this.nomComplet = nomComplet;
    }

    public String getRibBenificiare() {
        return ribBenificiare;
    }

    public void setRibBenificiare(String ribBenificiare) {
        this.ribBenificiare = ribBenificiare;
    }

    public String getMotifVersement() {
        return motifVersement;
    }

    public void setMotifVersement(String motifVersement) {
        this.motifVersement = motifVersement;
    }
}

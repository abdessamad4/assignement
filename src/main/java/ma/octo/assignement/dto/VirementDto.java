package ma.octo.assignement.dto;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class VirementDto {
  /**
   * AssignementComment:
   * ajout des validation des inputs
   * */

  @NotBlank(message = "champ numero de compte de l'Emetteur est obligatoire")
  private String nrCompteEmetteur;

  @NotBlank(message = "champ numero de compte du benificiaire est obligatoire")
  private String nrCompteBeneficiaire;

  @NotBlank(message = "champ motif est obligatoire")
  private String motif;

  @DecimalMin(value = "0.0", inclusive = false)
  @DecimalMax(value = "10000", inclusive = true)
  private BigDecimal montantVirement;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  @NotNull(message = "champ date est obligatoire")
  private Date date;

  public VirementDto(String nrCompteEmetteur, String nrCompteBeneficiaire, String motif, BigDecimal montantVirement, Date date) {
    this.nrCompteEmetteur = nrCompteEmetteur;
    this.nrCompteBeneficiaire = nrCompteBeneficiaire;
    this.motif = motif;
    this.montantVirement = montantVirement;
    this.date = date;
  }
  public VirementDto() {
  }
  @Override
  public String toString() {
    return "VirementDto{" +
            "nrCompteEmetteur='" + nrCompteEmetteur + '\'' +
            ", nrCompteBeneficiaire='" + nrCompteBeneficiaire + '\'' +
            ", motif='" + motif + '\'' +
            ", montantVirement=" + montantVirement +
            ", date=" + date +
            '}';
  }

  public String getNrCompteEmetteur() {
    return nrCompteEmetteur;
  }

  public void setNrCompteEmetteur(String nrCompteEmetteur) {
    this.nrCompteEmetteur = nrCompteEmetteur;
  }

  public String getNrCompteBeneficiaire() {
    return nrCompteBeneficiaire;
  }

  public void setNrCompteBeneficiaire(String nrCompteBeneficiaire) {
    this.nrCompteBeneficiaire = nrCompteBeneficiaire;
  }

  public BigDecimal getMontantVirement() {
    return montantVirement;
  }

  public void setMontantVirement(BigDecimal montantVirement) {
    this.montantVirement = montantVirement;
  }

  public String getMotif() {
    return motif;
  }

  public void setMotif(String motif) {
    this.motif = motif;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }
}

package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class CompteService {

    @Autowired
    private CompteRepository compteRepository;
    public List<Compte> getAllAccounts(){

            List<Compte> comptes = compteRepository.findAll();

            if (CollectionUtils.isEmpty(comptes)) {
                return null;
            } else {
                return comptes;
            }
    }

    public CompteDto addCompte(Compte compte){
        compteRepository.save(compte);
        return new CompteDto();
    }
}

package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.web.VersementController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Service
public class VersementService {

    @Autowired
    private VersementRepository versementRepository;
    Logger LOGGER = LoggerFactory.getLogger(VersementController.class);

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private AuditService auditService;
    public List<Versement> getAllDeposits(){
        List<Versement> versements = versementRepository.findAll();

        if (CollectionUtils.isEmpty(versements)) {
            return null;
        } else {
            return versements;
        }
    }

    public VersementDto addDeposit(VersementDto versementDto) throws CompteNonExistantException {
        Compte compteBenificiaire = compteRepository.findByRib(versementDto.getRibBenificiare());

        if(compteBenificiaire == null){
            LOGGER.error("Compte benificiaire Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }
        BigDecimal montantVersement=versementDto.getMontantVersement();
        BigDecimal soldeBenificiaire=compteBenificiaire.getSolde();
        /**
         * AssignementComment:
         * refactoring du code et reecriture des message de Logging
         * */

        //ajout du montant au solde du binificiaire
        compteBenificiaire.setSolde(soldeBenificiaire.add(montantVersement));
        compteRepository.save(compteBenificiaire);

        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDateExecution());
        versement.setCompteBeneficiaire(compteBenificiaire);
        versement.setMotifVersement(versementDto.getMotifVersement());
        versement.setNomComplet(versementDto.getNomComplet());
        versement.setMontantVersement(montantVersement);
        versementRepository.save(versement);

        // audit du versement
        auditService.auditTransaction(
                "Versement par " +
                        versementDto.getNomComplet() +
                        " vers " + versementDto.getRibBenificiare() +
                        " d'un montant de " + versementDto.getMontantVersement(),
                EventType.VERSEMENT
        );

        return versementDto;
    }
}

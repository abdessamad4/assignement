package ma.octo.assignement.service;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class UtilisateurService {

    @Autowired
    private UtilisateurRepository utilisateurRepository;
    public List<Utilisateur> getAllUsers(){

        List<Utilisateur> utilisateurs = utilisateurRepository.findAll();

        if (CollectionUtils.isEmpty(utilisateurs)) {
            return null;
        } else {
            return utilisateurs;
        }
    }
}

package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.web.VirementController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Service
public class VirementService {
    @Autowired
    private VirementRepository virementRepository;
    /**
     * AssignementComment:
     * Pas besoin de valider le montant : il vaut mieu valider les donnes dès leurs réception
     * donc on supprime la ligne suvante
     public static final int MONTANT_MAXIMAL = 10000;
     * */

    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private AuditService auditService;


    public List<Virement> getAllVirments(){
        List<Virement> virements = virementRepository.findAll();

        if (CollectionUtils.isEmpty(virements)) {
            return null;
        } else {
            return virements;
        }
    }

    public VirementDto addVirement(VirementDto virementDto) throws CompteNonExistantException, TransactionException {
        Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        /**
         * AssignementComment:
         * elimination du code dupliquee : if statement
         * */
        if (compteEmetteur == null || compteBeneficiaire == null) {
            /**
             * AssignementComment:
             * remplacer system.out.println par logger
             * */
            String typeCompteNull = compteEmetteur == null ? "Emetteur" : "Recepteur";
            LOGGER.error("Compte "+typeCompteNull+" Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }
        /**
         * AssignementComment:
         * Pas besoin de valider dans le service : il vaut mieu valider les donnes dès leurs réception
         * */
        BigDecimal soldeEmetteur = compteEmetteur.getSolde();
        BigDecimal soldeBeneficiaire = compteBeneficiaire.getSolde();
        BigDecimal montantVirement = virementDto.getMontantVirement();

        if (soldeEmetteur.compareTo(montantVirement)<0) {
            LOGGER.error("Solde insuffisant : le montant demandé est plus grand que le solde actuel");
            throw new TransactionException("Solde insuffisant");
        }
        /**
         * AssignementComment:
         * Elimination du duplication des lignes precedentes: verification if statement.
         * */

        /**
         * AssignementComment:
         * Il faut utiliser subtract et add au lieu de intValue sinon sa risque une perte d'argent.
         * */
        compteEmetteur.setSolde(soldeEmetteur.subtract(montantVirement));
        compteRepository.save(compteEmetteur);

        compteBeneficiaire.setSolde(soldeBeneficiaire.add(montantVirement));
        compteRepository.save(compteBeneficiaire);

        Virement virement = new Virement();
        virement.setDateExecution(virementDto.getDate());
        virement.setCompteBeneficiaire(compteBeneficiaire);
        virement.setCompteEmetteur(compteEmetteur);
        virement.setMontantVirement(virementDto.getMontantVirement());

        virementRepository.save(virement);

        // Modification : code refactoring
        auditService.auditTransaction(
                "Virement depuis " +
                        virementDto.getNrCompteEmetteur() +
                        " vers " + virementDto.getNrCompteBeneficiaire() +
                        " d'un montant de " + virementDto.getMontantVirement().toString(),
                EventType.VIREMENT
            );
        return virementDto;
        }
        /**
         * AssignementComment:
         * suppression de la methode save qui n'est pas utilisé
         * */
}

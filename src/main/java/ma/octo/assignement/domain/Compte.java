package ma.octo.assignement.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "COMPTE")
public class Compte {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 16, unique = true)
  private String nrCompte;
  /**
   * AssignementComment:
   * rib doit etre unique et a une taille de 30 charactere
   * */
  @Column(unique = true,length = 30)
  private String rib;

  @Column(precision = 10, scale = 2)
  private BigDecimal solde;

  @ManyToOne(fetch = FetchType.LAZY, targetEntity = Utilisateur.class)
  @JoinColumn(name = "utilisateur_id", insertable = false, updatable = false)
  private Utilisateur utilisateur;

  public Compte(String nrCompte, String rib, BigDecimal solde, Utilisateur utilisateur) {
    this.nrCompte = nrCompte;
    this.rib = rib;
    this.solde = solde;
    this.utilisateur = utilisateur;

  }

  public Compte() {
  }

  public String getNrCompte() {
    return nrCompte;
  }

  @Override
  public String toString() {
    return "Compte{" +
            "id=" + id +
            ", nrCompte='" + nrCompte + '\'' +
            ", rib='" + rib + '\'' +
            ", solde=" + solde +
            ", utilisateur=" + utilisateur +
            '}';
  }

  public void setNrCompte(String nrCompte) {
    this.nrCompte = nrCompte;
  }

  public String getRib() {
    return rib;
  }

  public void setRib(String rib) {
    this.rib = rib;
  }

  public BigDecimal getSolde() {
    return solde;
  }

  public void setSolde(BigDecimal solde) {
    this.solde = solde;
  }

  public Utilisateur getUtilisateur() {
    return utilisateur;
  }

  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}

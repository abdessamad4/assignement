package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
/**
 * AssignementComment:
 *
 * pour eviter les chauvechement entre les donnee des methodes de tests on recrer la base de donne apres chaque test
 * */
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class VersementServiceTest {

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private  VersementService versementService;

    Compte compte = new Compte();

    @BeforeEach
    void setUp() {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setUsername("abdoelo");
        utilisateur.setFirstname("abdo");
        utilisateur.setLastname("el wafi");
        utilisateur.setGender("Male");
        utilisateur.setBirthdate(new Date());
        utilisateurRepository.save(utilisateur);

        compte.setNrCompte("0000111100001111");
        compte.setRib("000011110000111100001111000011");
        compte.setSolde(BigDecimal.valueOf(200L));
        compte.setUtilisateur(utilisateur);

        compteRepository.save(compte);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAllDeposits() throws CompteNonExistantException {
        VersementDto versementDto=new VersementDto(
                BigDecimal.valueOf(1000.00),
                new Date(),
                "Youssef",
                "000011110000111100001111000011",
                "motif du versement");
        /**
         * AssignementComment:
         * s'assurer que effectivement le dernier montant versee c'est bien le montant entree
         * */
        versementService.addDeposit(versementDto);
        Versement versementRecent=versementService.getAllDeposits().get(0);
        Assertions.assertTrue( versementDto.getMontantVersement().compareTo(versementRecent.getMontantVersement()) == 0);
    }

    @Test
    void addDeposit() throws CompteNonExistantException {
        BigDecimal soldeBenificiaire = compte.getSolde();
        versementService.addDeposit(
                new VersementDto(BigDecimal.valueOf(1000),
                        new Date(),
                        "Youssef cheradi",
                        "000011110000111100001111000011",
                        "motif de versement")
        );

        BigDecimal montantVersement = new BigDecimal(1000);
        montantVersement= montantVersement.setScale(2);

        Assertions.assertEquals(
                soldeBenificiaire.add(montantVersement),
                compteRepository.findByNrCompte("0000111100001111").getSolde()
        );
    }
}
